# Patchnotes

## v1.2.3
- Changed the Dice So Nice! integration to let users pick their preferred dice preset instead of forcing the use of the DnD-UI dice.

## v1.2.2
- Improve Highlight on tools and scene navigation again ...

## v1.2.1
- Improve Highlight on tools and scene navigation

## v1.2.0
- Major Update

## v1.1.4
- Update for 0.7.5

## v1.1.3
- Fix invalid URL

## v1.1.2
- Dark Mode compatibility

## v1.1.1
- add Português (Brasil) translation, thx Mr @renato.innocenti

## v1.1.0
- New theme for dice: DnD5e Red Box

## v1.0.1
- Folder colors work now.

## v1.0.0
- Initial release 
